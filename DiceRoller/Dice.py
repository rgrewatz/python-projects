"""Stuff"""
import random
import inflect
class Dice:
    """Dice Class"""
    def __init__(self, sides):
        """Creating dice and sides Outside of method creates equivalent of static variables"""
        self.dice = []
        for i in range(0, sides):
            self.dice.append(inflect.engine().number_to_words(i+1))
        self.sides = sides
        print("SIDES: " + str(self.sides))
        print("DICE: " + str(len(self.dice)))

    def roll_dice(self):
        """Rolls Dice"""
        var = random.randint(0, self.sides-1)
        return self.dice[var]
