from Deck import Deck
class Player:

    def __init__(self, name=None):
        self.score = -1
        self.hand = []
        if name is None:
            name = ''
        self.player_name = name

    def take_card(self, card):
        self.hand.append(card)

class Dealer(Player):
    def __init__(self, name=None, deck=None):
        super(Dealer, self).__init__(name)
        self.deck = deck

    def shuffle_deck(self):
        self.deck.shuffle()

    def deal(self):
        if len(self.deck.cards) > 0:
            return self.deck.cards.pop()

class BlackJackDealer(Dealer):
    def __init__(self, name=None, deck=None):
        super(BlackJackDealer, self).__init__(name, deck)
        self.stop = False

class BlackJackPlayer(Player):
    def __init__(self, name=None):
        super(BlackJackPlayer, self).__init__(name)
        self.stop = False
