from Player import BlackJackPlayer
from Player import BlackJackDealer
from Deck import Deck
player_array = None
dealer = None
def initialize_game(numberOfPlayers, player_array, dealer):
    player_array = [BlackJackPlayer("Player " + str(i)) for i in range(1, numberOfPlayers+1)]
    dealer = BlackJackDealer("Dealer", Deck())
    dealer.shuffle_deck()
initialize_game(int(input('Please enter number of players: ')), player_array, dealer)
while len(dealer.deck.cards) > 0:
    for player in player_array:
        if player.score < 22:
            if player.stop is False:
                print(player.player_name + '\'s turn')
                result = -1
                if len(dealer.deck.cards) > 0 and player.score < 22:
                    while result != '0' and result != '1':
                        print(player.player_name + " Score: " + str(player.score))
                        result = input('Hit (1) or No (0): ')
                        if result == '1':
                            card = dealer.deal()
                            player.take_card(card)
                            player.score += card
                        elif result != '0':
                            player.stop = True
                            print('Invalid input')
                if player.score < 22:
                    print(player.player_name + " : " + str(player.score))
                else:
                    print(player.player_name + " : BUST")
    print(dealer.player_name + '\'s turn')
    if len(dealer.deck.cards) > 0 and dealer.score < 17:
        card = dealer.deal()
        dealer.take_card(card)
        dealer.score += card
    print(dealer.player_name + " : " + str(dealer.score))
