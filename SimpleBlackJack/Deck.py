import random
class Deck:
    def __init__(self):
        self.cards = [(i%13) + 1 for i in range(0, 52)]

    def shuffle(self):
        random.shuffle(self.cards)
        return self
